﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace RestHash.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        [Route("week")]
        public IActionResult GetForecast()
        {
            int difficultyPoW = 3;
            if (!HasValidProofOfWork(this.Request, difficultyPoW))
            {
                return BadRequest("No Valid Proof of Work!");
            }

            var rng = new Random();
            var forecast = Enumerable.Range(1, 7).Select(index => RandomForecast(rng, index)).ToArray();
            return Ok(forecast);
        }

        [HttpGet]
        [Route("today")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult GetToday()
        {
            int difficultyPoW = 1;
            if (!HasValidProofOfWork(this.Request, difficultyPoW))
            {
                return BadRequest("No Valid Proof of Work!");
            }

            var rng = new Random();
            var today = RandomForecast(rng, 0);
            return Ok(today);
        }

        private WeatherForecast RandomForecast(Random rng, int daysFromNow)
        {
            return new WeatherForecast
            {
                Date = DateTime.Now.AddDays(daysFromNow),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            };
        }

        private bool HasValidProofOfWork(HttpRequest httpRequest, int difficultyPoW)
        {
            if(!httpRequest.Headers.TryGetValue("HashREST", out var headerValues))
            {
                return false;
            }

            var headerString = headerValues.Single();
            var sha256 = SHA256.Create();
            var hash = sha256.ComputeHash(Encoding.UTF8.GetBytes(headerString));

            return HashIsValid(hash, difficultyPoW);
        }

        private bool HashIsValid(byte[] hash, int difficultyPoW)
        {
            var valid = hash.Take(difficultyPoW).All(b => b == 0);
            return valid;
        }
    }
}
