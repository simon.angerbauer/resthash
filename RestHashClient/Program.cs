﻿using System;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace RestHashClient
{
    class MainClass
    {
        public static async Task Main(string[] args)
        {
            HttpClient httpClient = new HttpClient();
            
            Console.WriteLine("Today's Weather Forecast - Matching difficulty (1) - Result received");
            var todaysWeather = await RequestWeather(httpClient, "http://localhost:5000/weatherforecast/today", 1);
            PrintResponse(todaysWeather);

            Console.WriteLine("Week's Weather Forecast - Matching difficulty (3) - Result received");
            var weeksWeather = await RequestWeather(httpClient, "http://localhost:5000/weatherforecast/week", 3);
            PrintResponse(weeksWeather);

            Console.WriteLine("Week's Weather Forecast - Difficulty too small (0 instead of 3) - Error");
            weeksWeather = await RequestWeather(httpClient, "http://localhost:5000/weatherforecast/week", 0);
            PrintResponse(weeksWeather);
        }

        private static void PrintResponse(string response)
        {
            Console.WriteLine("------------------------");
            Console.WriteLine("Server Response:");
            Console.WriteLine(response);
            Console.WriteLine("------------------------");
            Console.WriteLine();
        }

        private static async Task<string> RequestWeather(HttpClient httpClient, string requestUri, int difficultyPoW)
        {
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, requestUri);
            var sha256 = SHA256.Create();

            var timeStamp = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
            var url = request.RequestUri.ToString();
            var random = RandomString(6);
            var counter = 0;

            byte[] hash;
            string headerValue;
            do
            {
                headerValue = $"{timeStamp};{url};{random};{counter}";
                hash = sha256.ComputeHash(Encoding.UTF8.GetBytes(headerValue));
                counter++;
            } while (!HashIsValid(hash, difficultyPoW));

            request.Headers.Add("HashREST", headerValue);
            var response = await httpClient.SendAsync(request);
            var result = await response.Content.ReadAsStringAsync();
            return result;
        }

        private static string RandomString(int length)
        {
            var random = new Random();
            const string chars = "abcdefghijklmnopqrstuvwxyz";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        private static bool HashIsValid(byte[] hash, int difficultyPoW)
        {
            var valid = hash.Take(difficultyPoW).All(b => b == 0);
            return valid;
        }
    }
}
